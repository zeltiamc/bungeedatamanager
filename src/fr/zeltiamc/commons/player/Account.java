package fr.zeltiamc.commons.player;

import fr.zeltiamc.api.language.Langs;

public class Account {

	private String uuid;
	private int coins;
	private String lang;
	private int id;
	private int rank;

	public Account() {}

	public Account(String uuid, int coins, int id, String lang, int rank){
		this.uuid = uuid;
		this.coins = coins;
		this.id = id;
		this.lang = lang;
		this.rank = rank;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() { return lang; }

	public void setLang(Langs lang) { this.lang = lang.name(); }

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Account clone(){
		return new Account(uuid, coins, id, lang, rank);
	}

}
