package fr.zeltiamc.gdatamanager;

import fr.zeltiamc.gdatamanager.core.AccountSaver;
import fr.zeltiamc.gdatamanager.data.management.redis.RedisAccess;
import fr.zeltiamc.gdatamanager.data.management.sql.DatabaseManager;
import fr.zeltiamc.gdatamanager.listeners.ProxyJoinListener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

/**
 * 
 * @author DarkRails
 * @version: 1.0.0
 *
 */

public class BungeeDataManager extends Plugin{

	private static BungeeDataManager instance;
	public static BungeeDataManager getInstance() {return instance;}
	
	public void onEnable(){
		
		System.out.println("********************************");
		System.out.println(" Enabling BungeeDataManager ...");
		System.out.println("********************************");
		
		instance = this;
		
		//Initialiser les connections
		DatabaseManager.initAllDatabaseConnections();
		RedisAccess.initRedis();
		AccountSaver.runAccountSaver();
		
		//Enregistrer les events
		PluginManager pm = this.getProxy().getPluginManager();
		pm.registerListener(this, new ProxyJoinListener());
		
	}

	public void onDisable(){
		//Save les comptes
		AccountSaver.saveAllAccount();
        //D�truitre les connections
		DatabaseManager.closeAllDatabaseConnections();
		RedisAccess.closeRedis();

	}
	
	
}
