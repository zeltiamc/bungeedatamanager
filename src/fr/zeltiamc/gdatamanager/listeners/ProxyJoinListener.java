package fr.zeltiamc.gdatamanager.listeners;

import fr.zeltiamc.commons.player.Account;
import fr.zeltiamc.gdatamanager.BungeeDataManager;
import fr.zeltiamc.gdatamanager.core.AccountProvider;
import fr.zeltiamc.gdatamanager.core.AccountSaver;
import fr.zeltiamc.gdatamanager.data.management.exceptions.AccountNotFoundException;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ProxyJoinListener implements Listener{

    @EventHandler
    public void onProxiedPlayerJoin(PostLoginEvent e){
        final ProxiedPlayer player = e.getPlayer();

        BungeeCord.getInstance().getScheduler().runAsync(BungeeDataManager.getInstance(), ()-> {

            final AccountProvider accountProvider = new AccountProvider(player);
            try {
                final Account account = accountProvider.getAccount();
                AccountSaver.removeAccount(account);
            } catch (AccountNotFoundException e1) {
                System.err.println(e1.getMessage());
                player.disconnect(new TextComponent("§cUne Erreur est survenue lors du chargement de votre compte \n §cVeuillez contacter un administrateur sur le teamspeak \n §cTS.ZELTIAMC.FR"));
            }

        });

    }

    @EventHandler
    public void onProxiedPlayerQuit(PlayerDisconnectEvent e){
        final ProxiedPlayer player = e.getPlayer();
        final AccountProvider provider = new AccountProvider(player);
        try {
            final Account account = provider.getAccount();
            new AccountSaver(account);
        } catch (AccountNotFoundException e1) {
            e1.printStackTrace();
        }
    }

}
