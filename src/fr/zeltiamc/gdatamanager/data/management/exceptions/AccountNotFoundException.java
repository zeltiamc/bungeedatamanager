package fr.zeltiamc.gdatamanager.data.management.exceptions;

import java.util.UUID;

public class AccountNotFoundException extends Exception{

	public AccountNotFoundException(UUID uuid){
		super("The Account ("+ uuid.toString() +") was not found");
	}
}
