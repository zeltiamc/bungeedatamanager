package fr.zeltiamc.gdatamanager.data.management.redis;

public class RedisCredentials {

	private String host;
	private String password;
	private int port;
	private String clientName;
	
	public RedisCredentials(String host, String password, int port){
		this(host, password, port, "bungee_redis_access");
	}
	
	public RedisCredentials(String host, String password, int port, String clientName){
		this.host = host;
		this.password = password;
		this.port = port;
		this.clientName = clientName;
	}
	
	public String toURI(){
		
		StringBuilder sb = new StringBuilder();
		sb.append("redis://").append(host).append(":").append(port);
		
		return sb.toString();
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getClientName(){
		return clientName;
	}
	
	
}
