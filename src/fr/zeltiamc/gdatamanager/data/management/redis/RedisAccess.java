package fr.zeltiamc.gdatamanager.data.management.redis;

import fr.zeltiamc.gdatamanager.data.management.config.ConfigList;
import net.md_5.bungee.config.Configuration;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class RedisAccess {

	private RedissonClient redissonClient;
	public static RedisAccess instance;
	private static Configuration conf = ConfigList.REDIS.getConfig();

	public RedisAccess(RedisCredentials credentials){
		instance = this;
		this.redissonClient = initRedisson(credentials);
	}
	
	public static void initRedis(){
		new RedisAccess(new RedisCredentials(conf.getString("host"), conf.getString("password"), conf.getInt("port"), conf.getString("client-name")));
	}
	
	public static void closeRedis(){
		RedisAccess.instance.getRedissonClient().shutdown();
	}
	
	private RedissonClient initRedisson(RedisCredentials credentials){
		final Config config = new Config();
		config.setUseLinuxNativeEpoll(true);
		config.setThreads(conf.getInt("thread"));
		config.setNettyThreads(conf.getInt("thread"));
		config.useSingleServer()
			.setAddress(credentials.toURI())
			.setPassword(credentials.getPassword())
			.setDatabase(conf.getInt("database"))
			.setClientName(credentials.getClientName());
		return Redisson.create(config);
	}
	
	public RedissonClient getRedissonClient(){
		return redissonClient;
	}
	
	
}
