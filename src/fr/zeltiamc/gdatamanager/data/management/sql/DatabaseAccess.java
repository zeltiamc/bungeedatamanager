package fr.zeltiamc.gdatamanager.data.management.sql;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import fr.zeltiamc.gdatamanager.data.management.config.ConfigList;

public class DatabaseAccess {

	private DatabaseCredentials credentials;
	private HikariDataSource hikariDatasource;
	
	public DatabaseAccess(DatabaseCredentials credentials){
		this.credentials = credentials;
	}
	
	private void setupHikariCP(){
		
		final HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setMaximumPoolSize(ConfigList.SQL.getConfig().getInt("pool-size"));
		hikariConfig.setJdbcUrl(credentials.toURI());
		hikariConfig.setUsername(credentials.getUser());
		hikariConfig.setPassword(credentials.getPassword());
		hikariConfig.setMaxLifetime(600000L);
		hikariConfig.setIdleTimeout(300000L);
		hikariConfig.setLeakDetectionThreshold(300000L);
		hikariConfig.setConnectionTimeout(10000L);
		
		this.hikariDatasource = new HikariDataSource(hikariConfig);
	}
	
	public void initPool(){
		setupHikariCP();
	}
	
	public void closePool(){
		this.hikariDatasource.close();
	}
	
	public Connection getConnection() throws SQLException{
		if(this.hikariDatasource == null){
			System.out.println("Not Connected to Database");
			setupHikariCP();
		}
		return this.hikariDatasource.getConnection();
	}
	
}
