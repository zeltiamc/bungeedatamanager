package fr.zeltiamc.gdatamanager.data.management.sql;

import fr.zeltiamc.gdatamanager.data.management.config.ConfigList;
import net.md_5.bungee.config.Configuration;

public enum DatabaseManager {

	ZELTIA_SERVER(new DatabaseCredentials(ConfigList.SQL.getConfig().getString("host"), ConfigList.SQL.getConfig().getString("database"), ConfigList.SQL.getConfig().getString("user"), ConfigList.SQL.getConfig().getString("password"), ConfigList.SQL.getConfig().getInt("port")));
	
	private DatabaseAccess databaseAccess;
	
	private DatabaseManager(DatabaseCredentials credentials) {
		this.databaseAccess = new DatabaseAccess(credentials);
	}
	
	public DatabaseAccess getDatabaseAccess(){
		return databaseAccess;
	}
	
	public static void initAllDatabaseConnections(){
		for(DatabaseManager dbm : values()){
			dbm.databaseAccess.initPool();
		}
	}
	
	public static void closeAllDatabaseConnections(){
		for(DatabaseManager dbm : values()){
			dbm.databaseAccess.closePool();
		}
	}
}
