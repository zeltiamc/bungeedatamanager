package fr.zeltiamc.gdatamanager.data.management.sql;

public class DatabaseCredentials {
	
	/****************************************
	 *         Stockage des donn�es         *
	 ****************************************/
	
	private String urlBase;
	private String host;
	private String dbName;
	private String user;
	private String password;
	private String args;
	private int port;

	public DatabaseCredentials(String host, String dbName, String user, String password, String args, int port){
		this("jdbc:mysql://", host, dbName, user, password, args, port);
	}
	
	public DatabaseCredentials(String host, String dbName, String user, String password, String args){
		this(host, dbName, user, password, args, 3306);
	}

	public DatabaseCredentials(String host, String dbName, String user, String password){
		this(host, dbName, user, password, "");
	}

	public DatabaseCredentials(String host, String dbName, String user, String password, int port){
		this(host, dbName, user, password, "", port);
	}
	
	public DatabaseCredentials(String urlBase, String host, String dbName, String user, String password, String args, int port){
		
		this.urlBase = urlBase;
		this.host = host;
		this.dbName = dbName;
		this.user = user;
		this.password = password;
		this.args = args;
		this.port = port;
		
	}
	
	public String toURI(){
		StringBuilder sb = new StringBuilder();
		
		sb.append(urlBase).append(host).append(":").append(port).append("/").append(dbName).append(args);
		
		return sb.toString();
	}
	
	public String getUser(){
		return user;
	}
	
	public String getPassword(){
		return password;
	}
	
	
	
}
