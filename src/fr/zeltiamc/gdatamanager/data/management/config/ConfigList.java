package fr.zeltiamc.gdatamanager.data.management.config;

import net.md_5.bungee.config.Configuration;

public enum ConfigList {

    SQL,
    REDIS;

    private Config config;

    ConfigList(){
        this.config = new Config(this.name().toLowerCase());
    }

    public Config get(){
        return config;
    }

    public Configuration getConfig(){
        return config.get();
    }

}
