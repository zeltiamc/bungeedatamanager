package fr.zeltiamc.gdatamanager.data.management.config;

import fr.zeltiamc.gdatamanager.BungeeDataManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class Config {

    private Configuration configuration;
    private String name;
    private static BungeeDataManager i = BungeeDataManager.getInstance();

    public Config(String name){
        this.name = name;
        copyDefault();
        try {

            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(i.getInstance().getDataFolder(), name + ".yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Configuration get(){
        return configuration;
    }

    public void save(){
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(i.getDataFolder(), this.name + ".yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyDefault(){

        File file = new File(i.getDataFolder(), name+".yml");
        if (!file.exists()) {
            try (InputStream in = i.getResourceAsStream(name+".yml")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
