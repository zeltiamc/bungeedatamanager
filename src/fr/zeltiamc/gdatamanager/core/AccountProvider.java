package fr.zeltiamc.gdatamanager.core;

import fr.zeltiamc.api.language.Langs;
import fr.zeltiamc.commons.player.Account;
import fr.zeltiamc.gdatamanager.data.management.exceptions.AccountNotFoundException;
import fr.zeltiamc.gdatamanager.data.management.redis.RedisAccess;
import fr.zeltiamc.gdatamanager.data.management.sql.DatabaseManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import java.sql.*;
import java.util.UUID;

public class AccountProvider {

	public static final String redisKey = "account:";
	public static final Account defaultAccount = new Account(UUID.randomUUID().toString(), 50, 0, "FR_FR", 0);
	
	private RedisAccess redisaccess;
	private ProxiedPlayer player;

	public AccountProvider(ProxiedPlayer player){
		this.player = player;
		this.redisaccess = RedisAccess.instance;
	}
	
	public Account getAccount() throws AccountNotFoundException{
		
		Account account = getAccountFromRedis();
		
		if(account == null){
			account = getAccountFromDatabase();
		}
		
		if(account == null){
			throw new AccountNotFoundException(player.getUniqueId());
		}
		
		sendAccountToRedis(account);
		
		return account;
	}
	
	private Account getAccountFromRedis(){
		final RedissonClient redissonClient = redisaccess.getRedissonClient();
		final String key = redisKey + this.player.getUniqueId().toString();
		final RBucket<Account> accountBucket = redissonClient.getBucket(key);
		return accountBucket.get();
	}
	
	private Account getAccountFromDatabase() {
		
		try {
			final Connection connection = DatabaseManager.ZELTIA_SERVER.getDatabaseAccess().getConnection();
			final PreparedStatement ps = connection.prepareStatement("SELECT * FROM global__players WHERE uuid = ?");
			ps.setString(1, player.getUniqueId().toString());
			final ResultSet rs = ps.executeQuery();
			if(rs.next()){
				final int id = rs.getInt("id");
				final int coins = rs.getInt("coins");
				final String lang = rs.getString("lang");
				final int rank = rs.getInt("rank");
				final Account account = new Account(player.getUniqueId().toString(), coins, id, lang, rank);
				account.setLang(Langs.valueOf(lang));
                connection.close();
                return account;
			}else{
                connection.close();
				final Account paccount = defaultAccount.clone();
				paccount.setUuid(player.getUniqueId().toString());
				final Connection con = DatabaseManager.ZELTIA_SERVER.getDatabaseAccess().getConnection();
				final PreparedStatement pst = con.prepareStatement("INSERT INTO global__players (uuid, coins, lang, rank) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
				pst.setString(1, player.getUniqueId().toString());
				pst.setInt(2, paccount.getCoins());
				pst.setString(3, paccount.getLang());
				pst.setInt(4, paccount.getRank());
				final int row = pst.executeUpdate();
				final ResultSet rst = pst.getGeneratedKeys();

				if(row > 0 && rst.next()){
					final int id = rst.getInt(1);
					paccount.setId(id);
				}
				con.close();
				return paccount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	private void sendAccountToRedis(Account account) {

		final RedissonClient rclient = redisaccess.getRedissonClient();
		final String key = redisKey + this.player.getUniqueId().toString();
		final RBucket<Account> accountBucket = rclient.getBucket(key);
		accountBucket.set(account);

	}
	
}
