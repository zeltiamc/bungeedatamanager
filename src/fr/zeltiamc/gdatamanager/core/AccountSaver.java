package fr.zeltiamc.gdatamanager.core;

import fr.zeltiamc.commons.player.Account;
import fr.zeltiamc.gdatamanager.BungeeDataManager;
import fr.zeltiamc.gdatamanager.data.management.redis.RedisAccess;
import fr.zeltiamc.gdatamanager.data.management.sql.DatabaseManager;
import net.md_5.bungee.BungeeCord;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class AccountSaver {

    private static HashMap<Account, AccountSaver> toSave = new HashMap<>();

    private Account account;
    private long time;

    public AccountSaver(Account account){
        this.account = account;
        toSave.put(account, this);
    }

    public static void saveAllAccount(){
        for(AccountSaver saver : toSave.values()){
            saver.save();
        }
    }

    public static void removeAccount(Account account){
        if(toSave.containsKey(account)){
            toSave.remove(account);
        }
    }

    public static void runAccountSaver(){
        BungeeCord.getInstance().getScheduler().schedule(BungeeDataManager.getInstance(), ()->{

            for(AccountSaver saver : toSave.values()){
                saver.addTime(5L);
            }

        }, 5L, TimeUnit.MINUTES);
    }

    private void addTime(long amount){
        this.time = time + amount;
        if(time >= 5){
            BungeeCord.getInstance().getScheduler().runAsync(BungeeDataManager.getInstance(), this::save);
        }
    }

    private void save() {

        final RedissonClient rclient = RedisAccess.instance.getRedissonClient();
        final String key = "account:" + account.getUuid();
        final RBucket<Account> accountBucket = rclient.getBucket(key);
        accountBucket.delete();

        try {
            final Connection connection = DatabaseManager.ZELTIA_SERVER.getDatabaseAccess().getConnection();
            final PreparedStatement ps = connection.prepareStatement("UPDATE global__players SET uuid=?,coins=?,langue=? WHERE id=?");
            ps.setString(1, account.getUuid());
            ps.setInt(2, account.getCoins());
            ps.setString(3, account.getLang());
            ps.setInt(4, account.getId());
            ps.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
